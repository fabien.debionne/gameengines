public class Board {
    public static void main(String[] args) {
        Map map = new Map(10, 10);
        Element hero = new Character("Personnage Principal", Object.class.getName(), "humain", "🧝");
        Element mechant = new Character("Mechant", Object.class.getName(), "marcheur blanc", "🧟");
        Element arbre = new Decor("🌳");
        map.addElements(hero, 1);
        map.addElements(mechant,15);
        map.addElements(arbre, 30);
        map.displayMap();
    }
}
