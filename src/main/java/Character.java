import java.util.Arrays;

public class Character extends Element{

    private String race;
    private String nom;
    private int niveau = 1;
    private int experience = 0;
    private int pointDeVieTotal = 10;
    private int getPointDeVieRestant = 10;
    private Item[] inventaire = new Item[3];

    public Character() {
    }

    public Character(String description, String type, String race, String nom) {
        super(description);
        this.race = race;
        this.nom = nom;
    }

    @Override
    public void displayName() {
        System.out.print(this.nom + " | ");
    }

    @Override
    public String toString() {
        return "Character{" +
                "race='" + race + '\'' +
                ", nom='" + nom + '\'' +
                ", niveau=" + niveau +
                ", experience=" + experience +
                ", pointDeVieTotal=" + pointDeVieTotal +
                ", getPointDeVieRestant=" + getPointDeVieRestant +
                ", inventaire=" + Arrays.toString(inventaire) +
                '}' +
                super.toString();
    }
}
