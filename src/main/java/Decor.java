public class Decor extends Element {

    private String nom;

    public Decor(String nom) {
        this.nom = nom;
    }

    @Override
    public void displayName() {
        System.out.print(this.nom + " | ");
    }
}
