public abstract class Element {

    private String description;
    private String type = Object.class.getName();

    public Element() {
    }


    public Element(String description) {
        this.description = description;
    }

     public abstract void displayName();

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "description: " + this.description
                + " type: " + this.type;
    }

}
