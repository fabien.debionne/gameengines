import java.util.Random;

public class Map {

    private final int sizeX;
    private final int sizeY;
    private final Element[][] map;

    public Map(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.map = new Element[sizeX][sizeY];
        initialization();
    }

    private void initialization() {
        for (int i = 0; i < this.map.length; i++){
            for (int j = 0; j < this.map[i].length; j++){
                this.map[i][j] = null;
            }
        }
    }

    public void displayMap(){
        System.out.println(this);
        System.out.println();
        for (int i = 0; i < map.length; i++){
            for (int j = 0; j < map[i].length; j++){
                if (map[i][j] == null){
                    System.out.print("case vide | ");
                } else {
                    map[i][j].displayName();
                }
            }
            System.out.println();
        }
    }

    public void addElements(Element element, int quantite){
        int count = 0;
        do {
            int min = 0;
            int max = 9;
            int range = max - min;
            int posX = new Random().nextInt(range);
            int posY = new Random().nextInt(range);

            while(map[posX][posY] != null){
                posX = new Random().nextInt(range);
                posY = new Random().nextInt(range);
            }
            map[posX][posY] = element;
            count++;
        } while (count < quantite);
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    @Override
    public String toString() {
        return "You are on a map of " + sizeX * sizeY + " boxes.";
    }
}
